'use strict';

class Item
{
  constructor({ id, value, count }){
    this.id = id;
    this.value = value;
    this.count = count;
  }

  static createRootItem(){
    return new Item({ id: Item.ROOT_ID, value: -1, count: 1 });
  }
}
Item.ROOT_ID = -1;

// ======================================================================================

class Node
{
  static create(item, parent = null){
    return new Node({
      uid: ++Node.CURRENT_AVAILABLE_UID,
      item: item,
      parent: parent
    });
  }

  constructor({ uid, item, parent }){
    this.uid = uid;
    this.item = item;
    this.parent = parent;
    this.children = [];
  }

  get itemId(){ return this.item.id; }
  get itemValue(){ return this.item.value; }

  createChild(item){
    let child = Node.create(item, this);
    this.children.push(child);
    return child;
  }

  totalValue(){
    let total = this.itemValue,
        currentNode = this.parent;

    while (!currentNode.isRoot()){
      total += currentNode.itemValue;
      currentNode = currentNode.parent;
    }

    return total;
  }

  isRoot(){
    return this.parent == null;
  }

  ancestryItemIds(){
    let ancestry = [],
        currentNode = this;

    while (currentNode.itemId != -1){
      ancestry.unshift(currentNode.itemId);
      currentNode = currentNode.parent;
    }

    return ancestry;
  }

  print(){
    let depth = this.ancestryItemIds().length,
        tab = '';

    for (let i = 0; i < depth; i++) tab += '--';

    console.log(`${tab}(${this.itemId})\$${this.itemValue}`);
    this.children.forEach((child) => {
      child.print();
    });
  }
}
Node.CURRENT_AVAILABLE_UID = -1;

// ======================================================================================

class Tree
{
  constructor(root){
    this.root = root;
  }

  nodesWithTotalValue(value){
    this._targetValue = value;
    this._matchingNodes = [];

    this._findOn(this.root);
    return this._matchingNodes;
  }

  _findOn(node){
    node.children.forEach((child) => {
      let childTotalValue = child.totalValue();

      if (childTotalValue == this._targetValue)
        this._matchingNodes.push(child);
      else if (childTotalValue < this._targetValue)
        this._findOn(child);
    });
  }
}

// ======================================================================================

class TreeBuilder
{
  constructor(items){
    this.root = Node.create(Item.createRootItem());
    this.items = items;
  }

  build(){
    this._build(this.root);
    return new Tree(this.root);
  }

  _build(node){
    let childItems = this._validChildItemsFor(node);

    childItems.forEach((item) => {
      let child = node.createChild(item);
      this._build(child);
    });
  }

  _validChildItemsFor(node){
    let items = [];

    this.items.forEach((item) => {
      if (item.id == node.itemId || item.value == node.itemValue){
        let occurrence = this._getItemOccurrenceCountOnAncestry({ node, item }),
            remainingOccurrence = item.count - occurrence;

        if (remainingOccurrence > 0) items.push(item);
      }
      else if (item.value > node.itemValue){
        items.push(item);
      }
    });

    return items;
  }

  _getItemOccurrenceCountOnAncestry({ node, item }){
    let count = 0,
        currentNode = node;

    while (currentNode.parent != null){
      if ( item.id == currentNode.itemId ) count++;
      currentNode = currentNode.parent;
    }

    return count;
  }
}

// ======================================================================================

class Agent
{
  constructor(me, counts, values, maxRounds, log){
    this.me = me;

    this.items = this._buildItems(counts, values);
    this.maxAskingValue = this._calculateMaxAskingValue();
    this.minAskingValue = this.maxAskingValue * Agent.MIN_OFFER_VALUE;
    this.currentAskingValue = this.maxAskingValue;

    let treeBuilder = new TreeBuilder(this.items);
    this.itemsTree = treeBuilder.build();

    this.rounds = maxRounds;
    this.currentRound = 0;

    this.log = log;

    this.log("<init>");
    this.log("  Items:");
    this.log(`    Counts: ${counts}`);
    this.log(`    Values: ${values}`);
    this.log("  Asking Value:");
    this.log(`    Max: ${this.maxAskingValue}`);
    this.log(`    Min: ${this.minAskingValue}`);
    this.log("</init>");
  }

  offer(o){
    this.currentRound++;
    let startTime = new Date();

    this.log('<offer>');
    this.log(`current round: ${this.currentRound}`);
    let myOffer = null,
        reward = 0;

    if (o){
      reward = this._rewardFrom(o);

      this.currentAskingValue = this.maxAskingValue;
      this.log(`Current Asking Value: ${this.currentAskingValue}`);
      this.log("  Opponent's Offer:");
      this.log(`    count: ${o}`);
      this.log(`    value: ${reward}`);

      if ( reward >= this.currentAskingValue ){
        this.log("  Accepting Opponent's Offer!");
        this.log('</offer>');
        return undefined;
      }
    }

    myOffer =
      this.currentRound == 1 ?
        this._generateMaxOffer() :
        this._generateCounterOffer(o);

    this.log(`My Offer: ${myOffer}`);

    this.myLastOfferMade = myOffer;

    this.lastOpponentOfferId = o ? o.join('') : null;

    let endTime = new Date() - startTime;

    this.log(`Duration: ${endTime}ms`);
    if ( this._rewardFrom(myOffer) == reward ){
      this.log("  Accepting Opponent's Offer!");
      this.log('</offer>');
      return undefined;
    }

    // If last round
    if (this.currentRound == this.rounds && this.me == 1){
      // Last chance to agree
      if ( this.me == 1 && reward >= this.minAskingValue ) return undefined;

      // Last chance to make an offer
    }

    this.log('</offer>');
    return myOffer;
  }

  _rewardFrom(offer){
    let total = 0;
    for (let id = 0; id < this.items.length; id++)
      total += this.items[id].value * offer[id];

    return total;
  }

  _generateMaxOffer(){
    this.log('  Generating Max Offer');

    let candidateNodes = this.itemsTree.nodesWithTotalValue(this.maxAskingValue),
        bestNode = null,
        shortestAncestry = 100;

    candidateNodes.forEach((node) => {
      let ancestryLength = node.ancestryItemIds().length;

      if (!bestNode || ancestryLength <= shortestAncestry){
        bestNode = node;
        shortestAncestry = ancestryLength;
      }
    });

    this.myLastOfferNode = bestNode;

    return this._generateOfferFrom(bestNode);
  }

  _generateCounterOffer(offer){
    this.log('  Generating Counter Offer');
    this.currentOffer = offer;
    let counterOffer = null;

    this.itemWeights = this._calculateItemWeights(offer);
    this.totalItemsWeight = 0;
    for (let i = 0; i < this.items.length; i++)
      this.totalItemsWeight += this.itemWeights[i] * this.items[i].count;
    this.maxOfferWeight = Math.round( this.totalItemsWeight * Agent.MAX_OFFER_WEIGHT );

    this.log(`    Item Weights: ${this.itemWeights}`);
    this.log(`    Item Weights Total: ${this.totalItemsWeight}`);
    this.log(`    Max Offer Weight: ${this.maxOfferWeight}`);

    this.bestCounterOffer = null;
    if ( this.currentAskingValue > this.minAskingValue ){
      counterOffer = this._createBestCounterOffer();

      this.log(`    End Asking Value: ${this.currentAskingValue}`);

      if (counterOffer){
        this.log(`    Counter Offer Weight: ${counterOffer.weight}`);
        this.myLastOfferNode = counterOffer.node;
        return this._generateOfferFrom(counterOffer.node);
      }
    }

    this.log('No Counter Offer Generated. Using Last Offer Made');
    return this.myLastOfferMade;
  }

  _createBestCounterOffer(){
    this.currentAskingValue--;
    this.log(`  Create Best Counter Offer for asking value = ${this.currentAskingValue}`);

    let offer = this._getBestCounterOffer();

    this.log('    Best Candidate Counter Offer');

    // Has available counter offer
    if (offer.node != null){
      this.log(`      ${offer.node.ancestryItemIds()}`);
      this.log(`      weight: ${offer.weight}`);

      if ( this._isBetterThanBestCounterOffer(offer) && !this._isDeadlock(offer.node) )
        this.bestCounterOffer = offer;
      else
        return this._createBestCounterOffer();
    }
    else {
      this.log('      nothing found!');
      let nextAskingValue = this.currentAskingValue - 1,
          tryAgain = (nextAskingValue >= this.minAskingValue);

      this.log(`      next asking value = ${nextAskingValue}`);
      this.log(`      try again? ${tryAgain}`);

      if (tryAgain) return this._createBestCounterOffer();

      this.log('      use current best counter offer');
      return this.bestCounterOffer || null;
    }

    // Best Counter Offer is unacceptable
    if (this.bestCounterOffer.weight > this.maxOfferWeight){
      this.log('      Best Counter Offer Weight is UNACCEPTABLE!');
      let nextAskingValue = this.currentAskingValue - 1,
          tryAgain = (nextAskingValue >= this.minAskingValue);

      this.log(`      next asking value = ${nextAskingValue}`);
      this.log(`      try again? ${tryAgain}`);
      if (tryAgain) return this._createBestCounterOffer();
    }

    this.log('      use current best counter offer');
    return this.bestCounterOffer;
  }

  _getBestCounterOffer(){
    this.log("      Finding Best Counter Offer Node....");

    let candidateNodes = this.itemsTree.nodesWithTotalValue(this.currentAskingValue),
        bestNode = null,
        lowestTotalWeight = this.totalItemsWeight,
        highestItemCount = 0;

    candidateNodes.forEach((node) => {
      let itemIds = node.ancestryItemIds(),
          numberOfItems = itemIds.length;
      let totalWeight = itemIds.reduce((total, id) => (total + this.itemWeights[id]), 0);

      this.log('        <candidate>');
      this.log(`          ${itemIds}`);
      this.log(`          weight: ${totalWeight}`);
      this.log(`          length: ${numberOfItems}`);
      this.log('        </candidate>');

      if (
        (totalWeight == lowestTotalWeight && numberOfItems > highestItemCount) ||
        (totalWeight < lowestTotalWeight)
      ){
        bestNode = node;
        lowestTotalWeight = totalWeight;
        highestItemCount = numberOfItems;
      }
    });

    return { node: bestNode, weight: lowestTotalWeight };
  }

  _isBetterThanBestCounterOffer(offer){
    if (this.bestCounterOffer == null) return true;
    return this.bestCounterOffer.weight > offer.weight;
  }

  _isDeadlock(node){
    if (this.myLastOfferNode == null) return false;
    return (this.myLastOfferNode.uid == node.uid) &&
      (this.lastOpponentOfferId == this.currentOffer.join(''));
  }

  _calculateItemWeights(offer){
    let weights = [];

    for (let id = 0; id < this.items.length; id++){
      let item = this.items[id],
          myCount = offer[id],
          opponentCount = item.count - myCount,
          weight = opponentCount / item.count;

      weights.push(weight);
    }

    return weights;
  }

  _generateOfferFrom(node){
    let offer = [],
        itemIds = node.ancestryItemIds();

    for (let i = 0; i < this.items.length; i++) offer[i] = 0;
    itemIds.forEach((id) => { offer[id]++; });

    return offer;
  }

  _buildItems(counts, values){
    let items = [];

    for (let id = 0; id < counts.length; id++){
      let item = new Item({ id: id, value: values[id], count: counts[id] });
      items.push(item);
    }

    return items;
  }

  _calculateMaxAskingValue(){
    return this.items.reduce((total, item) => (total + (item.count * item.value)), 0);
  }
};

// 50% of Total Item Weights
Agent.MAX_OFFER_WEIGHT = 50 / 100;

// 50% of Total Item Values
Agent.MIN_OFFER_VALUE = 50 / 100;

module.exports = Agent;

//let agent = new Agent(0, [1, 1, 4], [1, 5, 1], 5, console.log);
//agent.offer([0, 0, 0]);

//module.exports = agent;
