const https = require('https');
const ME = "e6deb9e2230e954d6b2fe98ae8e706b0";

function parse(json){
  let totalPlayers = 0,
      totalSessions = 0;

  let i = 0;
  for ( let id in json ){
    totalPlayers += ++i;
    totalSessions += json[id].all.sessions;
  }

  let aveSessions = totalSessions / totalPlayers,
      myStat = null,
      agreementRates = [],
      scoreRates = [];

  let bestScore = null,
      bestAgreement = null;

  console.log(`Total Players: ${totalPlayers}`);
  console.log(`Total Sessions: ${totalSessions}`);
  console.log(`Average Session: ${aveSessions}`);

  aveSessions = 10000;

  for ( let id in json ){
    let stat = json[id].all;

    stat.key = id;
    stat.agreementRate = (stat.agreements / stat.sessions) * 100,
    stat.scoreRate = stat.score / stat.sessions;

    if (id == ME) myStat = stat;
    //if (stat.sessions < aveSessions) continue;
    if (stat.sessions < 1400 || stat.sessions > 1500) continue;
    //if ( stat.sessions != 1417 ) continue;

    agreementRates.push(stat.agreementRate);
    scoreRates.push(stat.scoreRate);

    if ( !bestScore || bestScore.scoreRate < stat.scoreRate ) bestScore = stat;
    if ( !bestAgreement || bestAgreement.agreementRate < stat.agreementRate)
      bestAgreement = stat;
  }

  let aveAgreementRate =
    agreementRates.reduce((total, r) => ( total + r ), 0) / agreementRates.length;
  let aveScoreRate =
    scoreRates.reduce((total, r) => ( total + r ), 0) / scoreRates.length;

  console.log('Best Score');
  console.log(bestScore);
  console.log('');
  console.log('Best Agreement');
  console.log(bestAgreement);
  console.log('');
  console.log(`Ave Agreement Rate: ${aveAgreementRate}%`);
  console.log(`Ave Score Rate: ${aveScoreRate}/session`);
  console.log('');
  console.log('My Stat');
  console.log(myStat);
}

https.get('https://hola.org/challenges/haggling/scores/standard', (resp) => {
  let data = '';

  resp.on('data', (chunk) => {
    data += chunk;
  });

  resp.on('end', () => {
    let json = JSON.parse(data);
    parse(json);
  });
});
