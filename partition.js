'use strict';

class Node {
  constructor({ value, parent, depth, total }){
    this.value = value;
    this.parent = parent;
    this.depth = depth;
    this.total = total;
  }

  ancestry(){
    let node = this,
        result = [];

    while (node){
      result.push(node.value);
      node = node.parent;
    }

    return result;
  }
}

class Partitioner {
  partition(number, count = 2){
    this.results = [];
    this.number = number;
    this.count = count;

    for (let i = 0; i <= this.number; i++){
      let root = new Node({ value: i, depth: 1, parent: null, total: i });
      this._build(root);
    }

    return this.results;
  }

  _build(node){
    let newDepth = node.depth + 1;

    if (newDepth == this.count)
      this._buildLastNodes(node);
    else
      this._buildChildNodes(node);
  }

  _buildChildNodes(node){
    for (let child = node.value; child <= this.number; child++){
      if ( (node.total + child) < this.number ){
        let childNode = this._createChildNode(node, child);
        this._build(childNode);
      }
    }
  }

  _buildLastNodes(node){
    for (let child = node.value; child <= this.number; child++){
      if ( (node.total + child) == this.number ){
        let childNode = this._createChildNode(node, child);
        this.results.push( childNode.ancestry() );
      }
    }
  }

  _createChildNode(parent, value){
    return new Node({
      value: value,
      parent: parent,
      depth: parent.depth + 1,
      total: parent.total + value
    });
  }
}

let p = new Partitioner();
module.exports = p;
